import { Component } from '@angular/core';

import { StatusService } from '../services/status.service';
import { StatusInfo } from '../model/status-info';

@Component({
  selector: 'app-package-search',
  templateUrl: './status.component.html',
  providers: [ StatusService ]
})

export class StatusComponent {

  status$: StatusInfo;

  search() {
    console.log('A pedir...');
    
      this.searchService.search()
      .subscribe((data: StatusInfo) => this.status$ = {
          status:   data['status'],
          version:  data['version'],
          sonho:    data['sonho']
      });
  }

  constructor(private searchService: StatusService) { }

}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/