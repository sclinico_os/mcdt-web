import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// used to create fake backend
import { fakeBackendProvider } from './helpers/fake-backend';
import { JwtInterceptor } from './helpers/jwt.interceptor';


import { AppRoutingModule }     from './app-routing.module';
import { AppComponent }         from './app.component';

import { AppNavbarComponent }   from './navbar/navbar.component';
import { AnalisesComponent } from './analises/analises.component';
import { ExamesComponent } from './exames/exames.component';
import { TratamentosComponent } from './tratamentos/tratamentos.component';

import { StatusComponent } from './status/status.component';
import { HttpErrorHandler }     from './http-error-handler.service';
import { MedicacaoComponent } from './medicacao/medicacao.component';
import { LoginComponent } from './login/login.component';




@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    
    // import HttpClientModule after BrowserModule.
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header',
    }),
    NgbModule.forRoot()

    
  ],
  declarations: [
    AppComponent,
    AppNavbarComponent,
    AnalisesComponent,
    StatusComponent,
    ExamesComponent,
    TratamentosComponent,
    MedicacaoComponent,
    LoginComponent
  ],

  providers: [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },

      // provider used to create fake backend
      fakeBackendProvider
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
