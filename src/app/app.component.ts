import { Component } from '@angular/core';

import { AuthenticationService } from './services/authentication.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Prescrição de MCDTs';

  constructor(private authenticationService: AuthenticationService) {
  }

  isLogged() {
    return this.authenticationService.isLogged();
  }



}
