import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AnalisesComponent }    from './analises/analises.component';
import { StatusComponent }    from './status/status.component';
import { ExamesComponent } from './exames/exames.component';
import { TratamentosComponent } from './tratamentos/tratamentos.component';
import { MedicacaoComponent } from './medicacao/medicacao.component';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: '/analises', pathMatch: 'full',canActivate: [AuthGuard] },
  { path: 'analises', component: AnalisesComponent,canActivate: [AuthGuard]  },
  { path: 'exames', component: ExamesComponent,canActivate: [AuthGuard]  },
  { path: 'tratamentos', component: TratamentosComponent ,canActivate: [AuthGuard] },
  { path: 'medicacao', component: MedicacaoComponent,canActivate: [AuthGuard]  },
  { path: 'status', component: StatusComponent,canActivate: [AuthGuard]  },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
