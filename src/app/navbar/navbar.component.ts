import { Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { User } from '../model/user';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';





@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class AppNavbarComponent implements OnInit {
  users: User[];

  constructor(private router:Router,
                private userService: UserService,
                private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe(users => { 
      this.users = users; 
    });
  }

  logout() {
    console.log("LOGOUT");
    this.authenticationService.logout();
    this.router.navigateByUrl('/login');
    
  }

}
